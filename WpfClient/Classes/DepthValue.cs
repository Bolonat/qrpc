﻿using System;
using System.ComponentModel;

namespace WpfClient
{ 
    public interface IDepthValue
{
    double Depth { get; set; }
    double Value { get; set; }
}

    /// <summary>
    /// Пара значений, которая представляет точку на графике
    /// </summary>
    public class DepthValue : INotifyPropertyChanged, IDepthValue
    {
        private double _value;
        private double _depth;

        public DepthValue()
        {}

        public DepthValue(double depth, double value)
        {
            Depth = depth;
            Value = value;
        }

        public double Value
        {
            get { return _value; }
            set
            {
                _value = value;
                OnPropertyChanged("Value");
            }
        }

        public double Depth
        {
            get { return _depth; }
            set
            {
                _depth = value;
                OnPropertyChanged("Depth");
            }
        }

        /// <summary>
        /// Тестовый массив для проверки работоспособности приложения
        /// </summary>
        /// <returns></returns>
        public static DepthValue[] GetDepthValues()
        {
            var result = new DepthValue[]
            {
     new DepthValue() {Depth=2100, Value=2.5},
     new DepthValue() {Depth=2200, Value=4.5},
     new DepthValue() {Depth=2300, Value=6.5}
            };
            return result;
        }

/// <summary>
/// Сформировать из полученных по сети данных двумерный массив для построения графика
/// </summary>
/// <param name="arr"></param>
/// <returns></returns>
        public static DepthValue[] GetDepthValues(double[] arr)
        {
            int len = arr.Length/2;
            DepthValue[] depthVal=new DepthValue[len];
            int idx = 0;
            for (int i = 0; i < arr.Length; i = i + 2)
            {
                if (i < arr.Length)
                {
                    var value = new DepthValue();
                    value.Depth = arr[i];
                    value.Value = arr[i+1];
                    depthVal[idx] = value;
                    idx++;
                }
            } 
            return depthVal;
        }

        #region INotifyPropertyChangedImplementation

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName = null)
        {
            if (PropertyChanged != null) PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }

}