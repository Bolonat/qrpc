﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace WpfClient
{
    public static class Log
    {

        /// <summary>
        /// Полный путь к приложению
        /// </summary>
        public static string FullPath { get; set; }

        /// <summary>
        /// Полный путь к папке протоколов
        /// </summary>
        public static string FullPathToLogs { get; set; }

        /// <summary>
        /// Полный путь к папке дополнительных модулей
        /// </summary>
        public static string FullPathToSharedLibraries { get; set; }

        /// <summary>
        /// Создать каталог для файлов протоколов
        /// </summary>
        public static void CreateLogsDirectory()
        {
            FullPath = Assembly.GetExecutingAssembly().Location;
            Int32 k = FullPath.IndexOf("WpfClient.exe");
            FullPath = FullPath.Substring(0, k - 1);
            FullPathToLogs = FullPath + "\\Logs";
            if (!Directory.Exists(FullPathToLogs))
                Directory.CreateDirectory(FullPathToLogs);
            FullPathToSharedLibraries = FullPath + "\\" + "SharedLibraries";

        }

        /// <summary>
        /// Записать сообщение в протокол 
        /// </summary>
        /// <returns></returns>
        public static void WriteMes(string msg)
        {
            WriteMes(msg, "log.txt");

        }

        /// <summary>
        /// Записать сообщение в протокол 
        /// </summary>
        /// <returns></returns>
        public static void WriteMes(string msg, string nameFile)
        {
            try
            {
                DateTime dt = DateTime.Now;
                msg = dt.ToString("dd.MM.yyyy HH:mm:ss.ms ") + msg + "\n";
                string fileName = FullPathToLogs + "\\" + nameFile;

                // запись в файл
                using (FileStream fstream = new FileStream(fileName, FileMode.Append))
                {
                    // преобразуем строку в байты
                    byte[] array = System.Text.Encoding.Default.GetBytes(msg);
                    // запись массива байтов в файл
                    fstream.Write(array, 0, array.Length);
                }
            }
            catch(Exception e)
            {              
            }
        }
    }
}
