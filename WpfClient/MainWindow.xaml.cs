﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows;
using LiveCharts;
using LiveCharts.Configurations;
using LiveCharts.Wpf;
using ServiceReference;


namespace WpfClient
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ContractWCFClient client = null;
        CartesianMapper<DepthValue> yMapper;
        public MainWindow()
        {
            InitializeComponent();
            Log.CreateLogsDirectory();
            yMapper = Mappers.Xy<DepthValue>()
  .X(x => x.Value)
  .Y(x => x.Depth);
            Charting.For<DepthValue>(yMapper);

            LiveChart.Series = new SeriesCollection();

            //Отрисовка осей для заставки
            DrowAxisX(2000, 3000);
            DrowAxisY(2, 7);
          
            //ShowChart(DepthValue.GetDepthValues());
        }


        private void DrowAxisX(double MinValueX, double MaxValueX)
        {
            LiveChart.AxisX.Clear();
            LiveChart.AxisX = new AxesCollection();
            double Step1 = (MaxValueX - MinValueX) / 10 ;
            var xAxis1 = new Axis
            {
                LabelFormatter = x => (x).ToString(CultureInfo.InvariantCulture),
                FontSize = 12,
                //Foreground = Brushes.Transparent,
                Separator =
                {
                    Step = (MaxValueX-MinValueX)/10,
                    Stroke =System.Windows.Media.Brushes.Black,
                    StrokeThickness = 0.4,
                    //StrokeDashArray = new DoubleCollection {3}

                },
                Title = "Depth",
                IsMerged = false,
                MinValue = MinValueX,
                MaxValue = MaxValueX,
                DisableAnimations = true,
                Unit = 1
            };

            LiveChart.AxisX.Add(xAxis1);
        }

        private void DrowAxisY(double MinValueY, double MaxValueY)
        {
            LiveChart.AxisY.Clear();
            LiveChart.AxisY = new AxesCollection();
            var yAxis = new Axis
            {
                LabelFormatter = x => x.ToString(CultureInfo.InvariantCulture),
                FontSize = 12,
                //Foreground = Brushes.Transparent,
                Separator =
                {
                    Step = (MaxValueY-MinValueY)/5,
                    Stroke =System.Windows.Media.Brushes.Black,
                    StrokeThickness = 0.2,
                    //StrokeDashArray = new DoubleCollection {3}
                },
                Title = "Value",
                MinValue = MinValueY,
                MaxValue = MaxValueY,
                IsMerged = false,
                DisableAnimations = true,
                Unit = 1
            };
            LiveChart.AxisY.Add(yAxis);
        }

        public void ShowChart(DepthValue[] ChartValues)
        {
            try
            {
                if (ChartValues.Length == 0)
                    return;

                List<double> listDepth = new List<double>();
                List<double> listValue = new List<double>();
                for (int i = 0; i < ChartValues.Length; i++)
                {
                    listDepth.Add(ChartValues[i].Depth);
                    listValue.Add(ChartValues[i].Value);
                }

                if (listDepth.Count == 0 || listValue.Count == 0)
                    return;

                //Заполнить оси:
                double MinValueX = Math.Round(listValue.Min(), 0);
                double MaxValueX = Math.Round(listValue.Max(), 0);
                DrowAxisX(MinValueX, MaxValueX);

                double MinValueY = Math.Round(listDepth.Min(), 0);
                double MaxValueY = Math.Round(listDepth.Max(), 0);
                DrowAxisY(MinValueY, MaxValueY);

                LiveChart.Series.Clear();
                List<DepthValue> list = ChartValues.ToList();
                if (ChartValues.Length > 0)
                {
                    LiveChart.Series.Add(new LineSeries
                    {
                        Values = new ChartValues<DepthValue>(list),
                        ScalesXAt = 0,
                        LineSmoothness = 0.4,
                        PointGeometry = null,
                        Fill = System.Windows.Media.Brushes.Transparent,
                        //LineSmoothness = CurveSettings.Smoothing ? 1 : 0,
                        Title = "Это график"
                    });
                }
            }
            catch(Exception e)
            {
                Print(e);
            }
        }


        private void Print(string text)
        {
            string str = textBlock.Text;
            textBlock.Text = text + "\n"+ str;
            Log.WriteMes(text);
        }

        private void Print(Exception ex)
        {
            Print(ex.Message);
           // Print(ex.Source);
           // Print(ex.StackTrace);
        }


        private bool NewClient()
        {
            bool L = true;
            New.IsEnabled = false;
            try
            {
               
                Print("Ждите... Выполняется тестовое подключение к серверу... ");
                client = new ContractWCFClient();
                string test = client.testConnection();
                Print("Подключено успешно: " + test);
                
            }
            catch (Exception e)
            {
                Print(e);
                client = null;
                L = false;
            }
            New.IsEnabled = true;
            return L;
        }

        private void GetDataFromHost()
        {
            GetData.IsEnabled = false;
            try
            {

               Print("Получаю данные ....");
                if (client != null)
                {
                    double[] data = client.GetData();
                    DepthValue[] arr = DepthValue.GetDepthValues(data);
                    ShowChart(arr);
                    string s = "";
                    for (int i = 0; i < data.Length; i++)
                    {
                        s += data[i].ToString() + ",";
                    }
                    Print(s);
                    client.Close();
                    client = null;
                    Print("Соединение завершено");
                }
                else
                {
                    if (NewClient())
                        GetDataFromHost();
                }

            }
            catch (Exception e)
            {
                Print(e);
                client = null;
            }
            GetData.IsEnabled = true;
        }

        private void GetData_Click(object sender, RoutedEventArgs e)
        {
            GetDataFromHost();
        }

        private void New_Click(object sender, RoutedEventArgs e)
        {
            NewClient();
        }
    }
}
