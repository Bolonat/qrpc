﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.ServiceModel.Description;

namespace WindowsService
{
    public partial class Service : ServiceBase
    {
        private ServiceHost serviceHost = null;
        public Service()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            if (serviceHost != null) serviceHost.Close();

            string addressHTTP = Properties.Settings.Default.AddressHTTP;
            string addressTCP = Properties.Settings.Default.AddressTCP;

            Uri[] address_base = { new Uri(addressHTTP), new Uri(addressTCP) };
            serviceHost = new ServiceHost(typeof(Contract.ContractWCF), address_base);

            ServiceMetadataBehavior behavior = new ServiceMetadataBehavior();
            serviceHost.Description.Behaviors.Add(behavior);

            BasicHttpBinding binding_http = new BasicHttpBinding();
            serviceHost.AddServiceEndpoint(typeof(Contract.IContractWCF), binding_http, addressHTTP);
            serviceHost.AddServiceEndpoint(typeof(IMetadataExchange), MetadataExchangeBindings.CreateMexHttpBinding(), "mex");

            NetTcpBinding binding_tcp = new NetTcpBinding();
            binding_tcp.Security.Mode = SecurityMode.Transport;
            binding_tcp.Security.Transport.ClientCredentialType = TcpClientCredentialType.Windows;
            binding_tcp.Security.Message.ClientCredentialType = MessageCredentialType.Windows;
            binding_tcp.Security.Transport.ProtectionLevel = System.Net.Security.ProtectionLevel.EncryptAndSign;
            serviceHost.AddServiceEndpoint(typeof(Contract.IContractWCF), binding_tcp, addressTCP);
            serviceHost.AddServiceEndpoint(typeof(IMetadataExchange), MetadataExchangeBindings.CreateMexTcpBinding(), "mex");

            serviceHost.Open();
        }

        protected override void OnStop()
        {
            if (serviceHost != null)
            {
                serviceHost.Close();
                serviceHost = null;
            }
        }
    }
}
