﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.Data;

namespace Contract
{
    /// <summary>
    /// Интерфейс описывает контракт обмена данными
    /// </summary>
        [ServiceContract]
        public interface IContractWCF
        {
        [OperationContract]
        double[] GetData();

        [OperationContract]
         string testConnection();

        }
}
