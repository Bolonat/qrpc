﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace Contract
{
    /// <summary>
    /// Класс, реализующий контракт обмена данными 
    /// </summary>
    public class ContractWCF : IContractWCF
    {
        /// <summary>
        /// Реализация метода web-службы
        /// Передать набор данных 
        /// </summary>
        /// <returns></returns>
        public double[] GetData()
        {
            //Здесь нужно организовать загрузку данных из БД или из файла
            //Предположим, массив содержит показания датчиков для определенной глубины скважины
            double[] arr = {2100, 2.3,   2200, 3.5,   2300, 6.5, 2400, 4.5, 2500, 6.5, 2600, 8.3, 2700, 5.2, 2800, 3.4,2900,2.8, 3000, 4.6};           
            return arr;
        }
        /// <summary>
        /// Этот метод можно использовать, чтобы протестировать соединение
        /// </summary>
        /// <returns></returns>
        public string testConnection()
        {
            return "Соединение установлено";
        }
    }
}
